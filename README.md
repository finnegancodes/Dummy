# Dummy

![Dummy Sceenshot](https://gitlab.com/finnegancodes/Dummy/-/raw/bff991f8ec05c6da3fa4ed6a2cbc7f390abca18a/Screenshots/dummy.png)

## About

Dummy is, as the name suggests, a stupid plain text editor for iOS. There is not much to it, you can open text files, edit them, it autosaves them; it's a text editor. 

Surprisingly though, I couldn't find an app like Dummy on the App Store. At least one that doesn't contain ads or costs like 5€, which is a ridiculus price for something that is just a `UITextView` slapped in a view controller with some document opening and saving logic. And closed source of course, but I mean, there is not much code to opensource anyway. (joke, code should always be public no matter how small)

Well anyways, here is exactly that for free with no ads. And made with SwiftUI instead of UIKit, but the same concept.

## Requirements

| Platform | Minimum version  |
| ----- | ------ |
| iOS/iPadOS | 16.0 |