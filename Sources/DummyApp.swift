//
//  DummyApp.swift
//  Dummy
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI

@main
struct DummyApp: App {
    var body: some Scene {
        DocumentGroup(newDocument: TextDocument()) { configuration in
            EditorView(text: configuration.$document.text)
        }
    }
}
