//
//  Autocapitalization.swift
//  Dummy
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI

enum Autocapitalization: String, CaseIterable {
    case off
    case characters
    case words
    case sentences
    
    var value: TextInputAutocapitalization {
        switch self {
        case .characters:
            return .characters
        case .words:
            return .words
        case .sentences:
            return .sentences
        case .off:
            return .never
        }
    }
}
