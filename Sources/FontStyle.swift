//
//  FontStyle.swift
//  Dummy
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI

enum FontStyle: String, CaseIterable {
    case normal
    case monospaced
    case serif
    
    var value: Font.Design {
        switch self {
        case .normal:
            return .default
        case .monospaced:
            return .monospaced
        case .serif:
            return .serif
        }
    }
}
