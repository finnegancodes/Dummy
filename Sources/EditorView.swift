//
//  EditorView.swift
//  Dummy
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI

struct EditorView: View {
    
    @Binding var text: String
    
    @AppStorage("fontSize") private var fontSize: Int = 16
    @AppStorage("fontStyle") private var fontStyle: FontStyle = .normal
    @AppStorage("autocorrectionEnabled") private var autocorrectionEnabled = true
    @AppStorage("autocapitalization") private var autocapitalization: Autocapitalization = .sentences
    
    @FocusState private var editorFocused: Bool
    
    var body: some View {
        VStack {
            TextEditor(text: $text)
                .font(.system(size: CGFloat(fontSize), design: fontStyle.value))
                .autocorrectionDisabled(!autocorrectionEnabled)
                .textInputAutocapitalization(autocapitalization.value)
                .focused($editorFocused)
                .scrollDismissesKeyboard(.never)
        }
        .toolbar {
            ToolbarItem(placement: .topBarTrailing) {
                editorMenu
            }
            keyboardToolbar
        }
    }
    
    var editorMenu: some View {
        Menu {
            Section {
                Stepper("Font Size: \(fontSize)", value: $fontSize, in: 10...24, step: 1)
                Picker(selection: $fontStyle) {
                    ForEach(FontStyle.allCases, id: \.rawValue) { style in
                        Text(style.rawValue.capitalized)
                            .tag(style)
                    }
                } label: {
                    Label("Font Style", systemImage: "f.cursive")
                }
                .pickerStyle(.menu)
            }
            Section {
                Button {
                    autocorrectionEnabled.toggle()
                } label: {
                    Label(autocorrectionEnabled ? "Disable Autocorrection" : "Enable Autocorrection", systemImage: "character.cursor.ibeam")
                }
                Picker(selection: $autocapitalization) {
                    ForEach(Autocapitalization.allCases, id: \.rawValue) { option in
                        Text(option.rawValue.capitalized)
                            .tag(option)
                    }
                } label: {
                    Label("Autocapitalization", systemImage: "textformat")
                }
                .pickerStyle(.menu)
            }
        } label: {
            Label("Options", systemImage: "ellipsis.circle")
        }
    }
    
    var keyboardToolbar: some ToolbarContent {
        ToolbarItemGroup(placement: .keyboard) {
            Spacer()
            Button("Done") {
                editorFocused = false
            }
            .fontWeight(.semibold)
        }
    }
}

#Preview {
    EditorView(text: .constant(""))
}
