//
//  TextDocument.swift
//  Dummy
//
//  Created by Adam Oravec on 27/04/2024.
//

import SwiftUI
import UniformTypeIdentifiers

struct TextDocument: FileDocument {
    
    static var readableContentTypes: [UTType] = [.plainText]
    static var writableContentTypes: [UTType] = [.plainText]
    
    var text: String = ""
    
    init() {
        self.text = ""
    }
    
    init(configuration: ReadConfiguration) throws {
        if let data = configuration.file.regularFileContents {
            self.text = String(data: data, encoding: .utf8) ?? ""
        }
    }
    
    func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
        FileWrapper(regularFileWithContents: Data(text.utf8))
    }
}
